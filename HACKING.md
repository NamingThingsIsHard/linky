All you need is python >3.6 and pip to get started.

# Getting set up

```bash
# (Optional) Set up a virtual environment
pip3 install virtualenv
virtualenv venv
source venv/bin/activate

# Install linky deps and dev deps
pip3 install -r requirements.txt
pip3 install -r requirements.dev.txt

# Run tests
python3 -m unittest
```

