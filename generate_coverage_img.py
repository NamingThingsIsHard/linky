from collections import namedtuple
from pathlib import Path

import cairo
import mk_badge
from coverage import Coverage

Analysis = namedtuple("Analysis", [
    "filename",
    "n_executable",
    "n_missing"
])


def mk_selector(idx):
    """
    Makes an generator that selects the given index of each  item
    in the iterable given to the generator

    @type idx: int
    @rtype: callable
    """

    def select(iterable):
        for element in iterable:
            yield element[idx]

    return select


def select_right_color(value):
    """
    Select a color for the text on the right
    @param value: Should be between 0 and 100
    @type value: int
    @rtype: tuple[float,float,float]
    """
    i = max(int(round(value / 33)) - 1, 0)
    return (
        (194 / 255, 13 / 255, 13 / 255),  # Dark red
        (156 / 255, 125 / 255, 29 / 255),  # Dark yellow
        (36 / 255, 101 / 255, 8 / 255),  # Dark green
    )[i]


def get_average_coverage(analyses):
    """
    Calculate the percent, average coverage of all files

    @param analyses:
    @type analyses: list[Analysis]
    @return: Percentage
    @rtype: int
    """
    percentages_missing_named = []
    for anal in analyses:
        percentage = 1 - (anal.n_missing / anal.n_executable) if anal.n_executable else 1
        percentage = round(percentage * 100)
        filename = anal[0]
        percentages_missing_named.append((filename, percentage))
        # print("%s: \t%s" % (filename, percentage))
    percentages_covered = [
        anal[1] for anal in percentages_missing_named
    ]
    average_coverage = int(sum(percentages_covered) / len(percentages_covered))
    return average_coverage


def get_total_coverage_percent(analyses):
    """
    @param analyses:
    @type analyses: list[Analysis]
    @return: Percentage of total code covered
    @rtype: int
    """
    total_lines = sum(mk_selector(1)(analyses))
    total_misses = sum(mk_selector(2)(analyses))
    dec_percentage_miss = total_misses / total_lines
    dec_coverage = 1 - dec_percentage_miss
    total_coverage = int(round(100 * dec_coverage))
    return total_coverage


def draw_badge(left_text, value, left_color, filename):
    """
    Takes care of actually generating a file with the given text, color, etc.
    """
    button = mk_badge.SplitButton(
        mk_badge.ButtonSideConfig(
            left_text, fill_color=left_color
        ),
        mk_badge.ButtonSideConfig(
            "%s%%" % str(value),
            text_color=(0.7, 0.7, 0.7),
            fill_color=select_right_color(value)
        ),
        mk_badge.FontConfig("Arial", 15),
        10,
        padding=3
    )
    width, height = button.calc_dimensions()
    # pylint: disable=no-member
    with cairo.SVGSurface(filename, width, height) as surface:
        button.draw(cairo.Context(surface), width, height)
    print("Wrote %s" % filename)


def main():
    """
    Makes the buttons and deposits them in an images/ folder
    """
    coverage = Coverage()
    coverage.load()

    data = coverage.get_data()

    # * The file name for the module.
    # * A list of line numbers of executable statements.
    # * A list of line numbers of excluded statements.
    # * A list of line numbers of statements not run (missing from
    #   execution).
    # * A readable formatted string of the missing line numbers.
    analyses = [
        coverage.analysis2(measured_file)
        for measured_file in sorted(data.measured_files())
    ]

    analyses = [
        Analysis(anal[0][anal[0].index("linky"):], len(anal[1]), len(anal[3]))
        for anal in analyses
    ]
    Path("images").mkdir(exist_ok=True)

    # Total coverage calcs
    total_coverage = get_total_coverage_percent(analyses)
    print("total coverage: %s" % total_coverage)
    left_color = (93 / 255, 81 / 255, 96 / 255)
    draw_badge("Coverage", total_coverage, left_color, "images/coverage.svg")

    average_coverage = get_average_coverage(analyses)
    print(
        "average percentage covered: %s" % average_coverage
    )

    draw_badge("Avg coverage", average_coverage, left_color, "images/avg_coverage.svg")


if __name__ == "__main__":
    main()
