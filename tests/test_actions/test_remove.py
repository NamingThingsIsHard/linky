import shutil
from pathlib import Path

from linky import actions
from linky.utils import path_utils
from tests import TEST_DATA_PATH
from tests.base_test import BaseTest


# pylint: disable=missing-function-docstring
class CommonMixin:
    """
    Tests common to remove_all and remove_files

    Deleting files shouldn't reveal any differences.
    Only directories will exhibit different behavior when deleted in a tag folder.
    """

    ALL = False

    @classmethod
    def call_func(cls, path: Path):
        actions.remove(path, cls.ALL)

    def setUp(self) -> None:  # pylint: disable=invalid-name
        BaseTest.setUp(self)
        self.test_data_dir = TEST_DATA_PATH / "remove"

    def test_try_remove_base(self):
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        with self.assertRaisesRegex(ValueError, "base path"):
            self.call_func(self.test_dir / path_utils.BASE_NAME)

    def test_try_remove_link_root(self):
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        with self.assertRaisesRegex(ValueError, "link root"):
            self.call_func(self.test_dir)

    def test_try_remove_config(self):
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        with self.assertRaises(ValueError):
            self.call_func(self.test_dir / path_utils.BASE_NAME / path_utils.CONFIG_DIR_NAME)

    def test_remove_file_from_base(self):
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        self.call_func(
            self.test_dir / path_utils.BASE_NAME / "Last Christmas (2019)/subs/cmn.srt")
        self._compare_test_dir("removed_file")

    def test_remove_only_file_from_dir(self):
        """
        The file and the empty directory should be removed
        """
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        to_remove = self.test_dir / \
                    path_utils.BASE_NAME / \
                    "The Hurt Locker (2008)/The.Hurt.Locker.2008.1080p.BRrip.mkv"
        self.call_func(to_remove)
        self.assertExists(to_remove, False)
        self.assertExists(to_remove.parent, False)

    def test_remove_file_from_root(self):
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        self.call_func(self.test_dir / "Year/2019/Last Christmas (2019)/subs/cmn.srt")
        self._compare_test_dir("removed_file")

    def test_remove_dir_from_base(self):
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        self.call_func(self.test_dir / path_utils.BASE_NAME / "Uncanny Valley (2015)")
        self._compare_test_dir("removed_dir")

    def test_remove_dir_from_root(self):
        raise NotImplementedError()


class TestRemoveAll(CommonMixin, BaseTest):
    """
    Make sure the actions/remove.py isn't doing anything weird
    """

    def test_remove_dir_from_root(self):
        """Ensure deleting a dir in a tag root removes it all, everywhere"""
        shutil.copytree(TEST_DATA_PATH / "movies", self.test_dir, symlinks=True)
        self.call_func(self.test_dir / "Year/2015/Uncanny Valley (2015)")
        self._compare_test_dir("removed_dir")


# pylint: disable=missing-function-docstring
class TestRemoveFiles(CommonMixin, BaseTest):
    """
    Make sure actions.remove.remove_files only removes the things it should
    """

    def test_remove_dir_from_root(self):
        """
        Select a folder in one tag and delete it.
        Only the files with the tag should be deleted while files without the tag shouldn't.
        """
        shutil.copytree(TEST_DATA_PATH / "init/with_categories/init_already_categorized",
                        self.test_dir,
                        symlinks=True, )
        self.call_func(self.test_dir / "Watched/Unwatched/no_subs")
        self._compare_test_dir("removed_files")
