#!/bin/bash
# Exit at the first sign of trouble
set -euo pipefail

# Render twine non interactive if we're in the CI
options=""
if [[ -n "$CI_PROJECT_DIR" ]] ; then
    options="--non-interactive"
fi

rm -rf dist build || true
python setup.py sdist bdist_wheel
twine check dist/*
twine upload $options dist/*
