import shutil
import unittest
from filecmp import dircmp
from os import walk
from pathlib import Path

from linky.config import read_conf
from linky.utils import path_utils
from tests import TEST_DATA_PATH, TEST_DIR_PATH


# pylint: disable=invalid-name
class BaseTest(unittest.TestCase):
    """
    Common functions for tests
    """

    def setUp(self) -> None:
        self.test_data_dir = TEST_DATA_PATH
        self.test_dir = TEST_DIR_PATH / self.__class__.__name__ / self._testMethodName
        self.base_path = self.test_dir / path_utils.BASE_NAME

        # Attempt to use RAM directory for tests as there's many reads and writes
        if not TEST_DIR_PATH.exists() and Path("/dev/shm").exists():
            test_dir_shm = Path("/dev/shm/linky_test_dir")
            test_dir_shm.mkdir(exist_ok=True)
            TEST_DIR_PATH.symlink_to(test_dir_shm)

        if self.test_dir.exists():
            shutil.rmtree(self.test_dir)

    def assertFilesAreGoodSymlinks(self, directory, ignore_base=True, ignore_list=None):
        """
        Check if all symlinks resolve in the given directory

        :param directory:
        :type directory: Path
        :param ignore_base: Ignores the base directory
        :type ignore_base: bool
        :param ignore_list: Absolute paths to ignore
        :type ignore_list: list[Path]
        """
        ignore_list = ignore_list or []
        base_dir = directory / path_utils.BASE_NAME
        for curr_path, _, filenames in walk(directory):
            if ignore_base and curr_path.startswith(str(base_dir)):
                continue
            curr_path = Path(curr_path)
            for filename in filenames:
                path_filename = curr_path / filename
                if path_filename in ignore_list:
                    continue
                self.assertTrue(path_filename.is_symlink(),
                                "%s is not a symlink" % path_filename)
                self.assertTrue(path_filename.exists(),
                                "%s is an invalid symlink" % path_filename)

    def assertLenEqual(self, iterable, length):
        """
        Make sure the iterable has the given length
        """
        self.assertEqual(len(list(iterable)), length)

    def assertDirCmpIsOK(self, comparator, top_left, top_right, cur_dir):
        """
        :type comparator: dircmp
        """
        self.assertEqual(len(comparator.left_only), 0, "Files only on left %s: %s" % (
            top_left / cur_dir, comparator.left_only
        ))
        self.assertEqual(len(comparator.right_only), 0, "Files only on right %s: %s" % (
            top_right / cur_dir, comparator.right_only
        ))

        for path, subdir_comparator in comparator.subdirs.items():
            self.assertDirCmpIsOK(
                subdir_comparator,
                top_left, top_right,
                "%s/%s" % (cur_dir, path)
            )

    def _compare_test_dir(self, compare_dir_name):
        top_left = self.test_data_dir / compare_dir_name
        comparator = dircmp(
            top_left,
            self.test_dir
        )
        self.assertDirCmpIsOK(comparator, top_left, self.test_dir, "")

    def assertExists(self, path, expected=True):
        """
        Make sure the give file's existence equals the expected
        """
        self.assertEqual(
            path.exists(), expected,
            "Expected '%s' to exist: %s" % (
                path, expected
            )
        )


class FixtureBaseTest(BaseTest):
    """
    Tests that use a fixture copied into their test directory
    """

    FIXTURE_NAME = "movies"
    INITIALIZED = True
    COPY_SYMLINKS = True

    def setUp(self) -> None:
        super().setUp()
        fixture_path = TEST_DATA_PATH / self.FIXTURE_NAME
        shutil.copytree(fixture_path, self.test_dir, symlinks=self.COPY_SYMLINKS)

        self.config = read_conf(self.test_dir) if self.INITIALIZED else None
