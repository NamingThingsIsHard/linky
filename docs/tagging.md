# Tagging 

Tagging is basically the action of putting a linked file or folder
 into a corresponding category and tag folder.

Use `linky tag` to assign a category and tag to a given file or folder.

## Resulting heirarchy

`<root>/<category>/<tag>/<content>`

**Example**

 - `./Rating/1/George Capon - Breathe.mp3`
 - `./Artist/George Capon/George Capon - Breathe.mp3`
 - `./Actor/Felicia Day/The Guild/S0101 - Wake-Up Call.mp4`
