import shutil
from filecmp import dircmp
from unittest.case import skip

from linky import app
from linky.actions import init
from tests import TEST_DATA_PATH
from tests.base_test import BaseTest


@skip("Toggling tags will be implemented later")
# pylint: disable=missing-function-docstring
class TestToggleWatched(BaseTest):
    """
    Tests toggling a tag
    """

    def setUp(self):
        BaseTest.setUp(self)
        self.noinit_path = TEST_DATA_PATH / "to_init_with_watched"
        shutil.copytree(self.noinit_path, self.test_dir)
        init(self.test_dir)

    # pylint: disable=invalid-name
    def assertDirEqual(self, left, right):
        self.assertLenEqual(dircmp(left, right).diff_files, 0)

    def test_toggle_watch_existing_file(self):
        source_main = self.test_dir / "Watched" / "main"
        # pylint: disable=no-member
        app.toggle_watch(source_main)

        expected_path = self.test_dir / "Unwatched" / "main"
        self.assertTrue(expected_path.exists())
        self.assertTrue(expected_path.is_symlink())
        self.assertFalse(source_main.exists())
        self.assertFilesAreGoodSymlinks(self.test_dir)

    def test_toggle_watch_existing_dir(self):
        source_main = self.test_dir / "Watched" / "with_subs"
        # pylint: disable=no-member
        app.toggle_watch(source_main)

        expected_path = self.test_dir / "Unwatched" / "with_subs"
        self.assertTrue(expected_path.exists())

        # Check folder structure
        self.assertDirEqual(
            self.noinit_path / "Watched" / "with_subs",
            expected_path
        )
        self.assertFilesAreGoodSymlinks(self.test_dir)

    def test_toggle_watch_non_existent_file(self):
        source_main = self.test_dir / "Watched" / "doesn't exist"
        # pylint: disable=no-member
        self.assertRaises(FileNotFoundError, app.toggle_watch, source_main)

    def test_toggle_watch_Watched(self):
        """
        Test toggling an entire "Watched" folder
        """
        watched_path = self.test_dir / "Watched"
        # pylint: disable=no-member
        app.toggle_watch(watched_path)

        unwatched_path = self.test_dir / "Unwatched"
        self.assertTrue(unwatched_path.exists())

        self.assertTrue(watched_path.exists())
        self.assertLenEqual(watched_path.iterdir(), 0)

        self.assertDirEqual(
            self.noinit_path / "Watched" / "with_subs",
            unwatched_path
        )
