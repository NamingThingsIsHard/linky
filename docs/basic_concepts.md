# Basic concepts

`linky` is the name of the program behind all this.
As mentioned in the [README], it uses a base directory and symlinks 
 to link to the content within the directory.
 
Here are some additional basic concepts.

## Symlinks

These are a concept of filesystems wherein the item points to a file or directory.

Here's an example directory structure

```
.
├── link_to_OG_file -> sub_directory/another_OG_file
├── link_to_original -> original_file
├── original_file
└── sub_directory
    ├── another_link_to_original_file -> ../original_file
    └── another_OG_file
```

## Initializing / Linking up

In order to have `linky` manage anything, it needs to have a certain directory structure.
This is achieved by `linky init <dir>`.

A root directory managed by `linky` is called a [Linked Root].

## Categories and tags

The whole goal of `linky` is the organizing files and folders by tagging them 
 and having those tags be part of the hierarchy when browsing those files and folders.

Tags have "parents" which are called [Categories].
Categories are used to group tags themselves, therefore categories can also be thought
 of as tag groups.
 
Categories are defined by [Linked Root] in their [Configuration].
This means there's no fixed, global definition of categories.
Each linked root will define its own set of categories.

Read [Tagging] to see how to tag 

[Categories]: ./categories.md
[Configuration]: ./configuration.md
[Linked Root]: ./glossary.md#linked-root
[README]: ../README.md
[Tagging]: tagging.md
