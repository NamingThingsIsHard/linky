#  Categories

Or tag groups, will define: 

## Availability and extensibility

Which tags are available and whether a user can add more tags.

**Examples**:

  - _Nonextensible_:
    - Rating -> Unrated,1,2,3,4,5
    - Watched -> Watched, Unwatched
  - _Extensible_:
    - Actors
    - Artist
    - Featured Artist

## Defaults

Which default tag(s) are assigned to new content in the linked root.

It's of course possible not to have a default.

**Examples:**

  - Rating -> Unrated
  - Tags -> Untagged

## Exclusivity

Whether multiple tags can be assigned to an item.

**Examples:**

  - _Exclusive categories_:
    - Artist
    - Rating
  - _Unexclusive categories_:
    - Actors
    - Featured Artists
    - Genres

