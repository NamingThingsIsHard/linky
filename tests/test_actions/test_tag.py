from os import walk
from pathlib import Path

from linky.actions import tag
from linky.utils import path_utils
from linky.utils.path_utils import CategoryTagTuple
from tests.base_test import FixtureBaseTest


# pylint: disable=missing-function-docstring
class TestTag(FixtureBaseTest):
    """
    Tests for actions/tag.py
    """

    def test_add_non_exclusive_tag_to_file(self):
        self._test_add_tag(
            Path("The Hurt Locker (2008)", "The.Hurt.Locker.2008.1080p.BRrip.mkv"),
            CategoryTagTuple("Actors", "Suhail Dabbach"),
            CategoryTagTuple("Watched", "Watched")
        )

    def test_add_non_exclusive_tag_to_dir(self):
        self._test_add_tag(
            Path("The Hurt Locker (2008)"),
            CategoryTagTuple("Actors", "Suhail Dabbach"),
            CategoryTagTuple("Watched", "Watched")
        )

    def test_add_non_exclusive_tag_to_dir_with_subdir(self):
        self._test_add_tag(
            Path("Last Christmas (2019)"),
            CategoryTagTuple("Actors", "Maxim Baldry"),
            CategoryTagTuple("Watched", "Unwatched")
        )

    # pylint: disable=too-many-locals
    def _test_add_tag(self, file_or_folder, new_cat_tag, old_cat_tag,
                      expected_exclusive_deletions=0):
        """
        Tests adding a non-exclusive tag to a file or directory

        @param file_or_folder: A relative path to the file or folder that should be tagged
        @type file_or_folder: Path
        @type new_cat_tag: CategoryTagTuple
        @type old_cat_tag: CategoryTagTuple
        """
        old_tag_root = self.test_dir / old_cat_tag.c / old_cat_tag.t
        new_tag_root = self.test_dir / new_cat_tag.c / new_cat_tag.t

        old_other = old_tag_root / file_or_folder
        expected_new_path = new_tag_root / file_or_folder
        old_path_props = path_utils.get_path_props(old_other)

        rel_content_paths = []
        old_others = path_utils.get_paths_in_root(old_other, self.config)
        if old_other.is_dir():
            # List the contents of the old dir
            for root, dirs, files in walk(old_other):
                for item in [*dirs, *files]:
                    current_path = Path(root) / item
                    rel_content_paths.append(current_path.relative_to(old_other))

        # Apply the new tag
        tag(old_other, str(new_cat_tag), self.config)

        self.assertTrue(expected_new_path.exists(),
                        "New path doesn't exist: '%s'" % expected_new_path)
        if not expected_exclusive_deletions:
            self.assertDictEqual(
                old_path_props,
                path_utils.get_path_props(expected_new_path),
                "Path props of '%s' and '%s' don't match" % (
                    old_other,
                    expected_new_path
                )
            )

        # Make sure the old contents were correctly copied to the new tag
        for rel_content_path in rel_content_paths:
            expected_item = expected_new_path / rel_content_path
            self.assertTrue(expected_item.exists(),
                            "Expected item doesn't exist: '%s'" % expected_item)
            if expected_item.is_file():
                self.assertTrue(expected_item.is_symlink(),
                                "Expected item isn't a symlink: '%s'" % expected_item)

        if expected_exclusive_deletions:
            # Make sure the other paths in the same category have been deleted
            exclusive_deletions = 0
            for old_other in old_others:
                rel_old = old_other.relative_to(self.test_dir)
                cat_tag = CategoryTagTuple(rel_old.parts[0], rel_old.parts[1])
                # Don't check tags in different categories
                # Or the cat/tag
                older_exists = old_other.exists()
                if cat_tag.c != new_cat_tag.c or cat_tag == new_cat_tag:
                    self.assertTrue(
                        older_exists,
                        "Exclusive tag %s shouldn't delete old tag %s" % (
                            repr(new_cat_tag),
                            repr(cat_tag),
                        )
                    )
                else:
                    self.assertFalse(older_exists,
                                     "Exclusive tag didn't delete old tag %s" % repr(cat_tag))
                    exclusive_deletions += 1

            if old_others:
                self.assertEqual(
                    exclusive_deletions,
                    expected_exclusive_deletions,
                    "Unexpected exclusive deletion count"
                )

        self.assertFilesAreGoodSymlinks(self.test_dir)

    def test_should_pass_with_exclusive_tag(self):
        self._test_add_tag(
            Path("Last Christmas (2019)"),
            CategoryTagTuple("Watched", "Watched"),
            CategoryTagTuple("Watched", "Unwatched"),
            1
        )

    def test_should_pass_with_exclusive_tag_between_categories(self):
        self._test_add_tag(
            Path("Last Christmas (2019)"),
            CategoryTagTuple("Watched", "Watched"),
            CategoryTagTuple("Actors", "Emilia Clarke"),
            1
        )

    def test_should_pass_default_exclusive_tag_with_multiple_predecessors(self):
        self._test_add_tag(
            Path("Last Christmas (2019)"),
            CategoryTagTuple("Actors", "None"),
            CategoryTagTuple("Actors", "Emilia Clarke"),
            3
        )

    def test_should_pass_new_tag_after_default_exclusive_tag(self):
        """
        Make sure the exclusive default tag is removed once a new tag is applied
        """
        self._test_add_tag(
            Path("Starcraft - Voldemort and the Sith (2020)"),
            CategoryTagTuple("Actors", "Lara Croft"),
            CategoryTagTuple("Actors", "None"),
            1
        )

    def test_should_pass_with_same_exclusive_tag(self):
        """
        Pass the same tag as the original tag
        It shouldn't have an effect
        """
        self._test_add_tag(
            Path("Last Christmas (2019)"),
            CategoryTagTuple("Watched", "Unwatched"),
            CategoryTagTuple("Watched", "Unwatched"),
            0
        )

    def _test_delete_to_default(self, movie, new_cat_tag, old_cat_tag):
        path = self.test_dir / old_cat_tag.c / old_cat_tag.t / movie
        expected_path = self.test_dir / new_cat_tag.c / new_cat_tag.t / movie
        tag(path, str(old_cat_tag), self.config, delete=True)
        self.assertFalse(path.exists(), "Old path still exists: %s" % path)
        self.assertTrue(expected_path.exists(), "New path doesn't exist")
        self.assertFilesAreGoodSymlinks(self.test_dir)

    def test_should_pass_deleting_exclusive_tag(self):
        """
        Deleting an exclusive tag should apply the default tag
        """
        old_cat_tag = CategoryTagTuple("Watched", "Watched")
        new_cat_tag = CategoryTagTuple("Watched", "Unwatched")

        movie = "The Hurt Locker (2008)"

        self._test_delete_to_default(movie, new_cat_tag, old_cat_tag)

    def test_should_pass_default_after_last_tag_deletion(self):
        """
        Deleting the last non-exclusive tag should apply the default tag
        """
        old_cat_tag = CategoryTagTuple("Actors", "Marcela Sandra Ballestero")
        new_cat_tag = CategoryTagTuple("Actors", "None")

        movie = "Uncanny Valley (2015)"

        self._test_delete_to_default(movie, new_cat_tag, old_cat_tag)

    def test_should_pass_tag_deletion(self):
        """
        Removing a non-exclusive tag with others left
        """
        cat_tag = CategoryTagTuple("Actors", "Jeremy Renner")
        movie = "The Hurt Locker (2008)"
        path = self.test_dir / cat_tag.c / cat_tag.t / movie

        current_paths = path_utils.get_paths_in_root(path, self.config, cat_tag.c)

        self.assertLenEqual(current_paths, 3)
        tag(path, str(cat_tag), self.config, delete=True)

        new_paths = path_utils.get_paths_in_root(self.test_dir / path_utils.BASE_NAME / movie,
                                                 self.config, cat_tag.c)
        self.assertFalse(path.exists(), "Old path still exists")
        self.assertLenEqual(new_paths, 2)

    def test_multi_tag_dir(self):
        """
        Tagging a directory that contains some files in the target cat/tag should pass
        """
        sub_dir = "Actors/Emilia Clarke/Last Christmas (2019)/subs/"
        # First tag one file in a directory
        cat_tag = ["Actors/Guy Pearce"]
        tag(
            self.test_dir / sub_dir / "de.srt", cat_tag, self.config
        )
        # Now try tagging the whole dir
        tag(
            self.test_dir / sub_dir, cat_tag, self.config
        )
        self._compare_test_dir("tag/after_test_multi_tag_dir")

    def test_should_tag_file_with_multiple_tags(self):
        tag(self.test_dir / "Watched" / "Unwatched" / "Starcraft - Voldemort and the Sith (2020)", [
            "Watched/Watched", "Actors/Voldi", "Year/420"
        ], self.config)
        self._compare_test_dir("tag/after_file_with_multiple_tags")

    def test_should_fail_with_nonexistent_path(self):
        self.assertRaises(
            FileNotFoundError,
            tag, self.test_dir / "OMEGAKEK",
            "Actors/Haily Sweeney", self.config
        )

    def test_should_fail_with_nonexistent_category(self):
        path = self.test_dir / \
               "Actors" / \
               "Madison Ingoldsby" / \
               "Last Christmas (2019)"
        self.assertRaisesRegex(
            ValueError, "^Unknown category",
            tag, path,
            "I do not exist/Haily Sweeney", self.config
        )

    def test_should_fail_with_nonexistent_tag(self):
        path = self.test_dir / \
               "Actors" / \
               "Madison Ingoldsby" / \
               "Last Christmas (2019)"
        self.assertRaisesRegex(
            ValueError, "^Unknown tag",
            tag, path,
            "Watched/LMAOWUT?", self.config
        )

    def test_should_fail_with_base(self):
        """
        Trying to tag the base should fail
        """
        self.assertRaisesRegex(
            ValueError, "base",
            tag, self.test_dir / path_utils.BASE_NAME,
            "Actors/Haily Sweeney", self.config
        )

    def test_should_fail_with_root(self):
        """
        Trying to tag the base should fail
        """
        self.assertRaisesRegex(
            ValueError, "root",
            tag, self.test_dir,
            "Actors/Haily Sweeney", self.config
        )
