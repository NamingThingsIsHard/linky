Configuration is per linked root and stored in a `.linky` directory.

In uninitialized folders `.linky` should be in the root.
Once initialized it's stored in `.base` --> `.base/.linky` and not linked - 
 that means it's only in the base directory.
 
Most files in the configuration directory will be [YAML] files. 
Their corresponding schemas are validated using [yamale]

## .linky/main.yaml

[main.schema.yaml][]

Configuration for the behavior of linky is stored here.
That includes things like the addition of a standard prefix amongst other things.

## .linky/categories.yaml

[categories.schema.yaml][]

This file contains all categories and their definitions.
It is documented too 😉


[YAML]: https://yaml.org/
[categories.schema.yaml]: /linky/schemas/categories.schema.yaml
[main.schema.yaml]: /linky/schemas/main.schema.yaml
[yamale]: https://github.com/23andMe/Yamale
