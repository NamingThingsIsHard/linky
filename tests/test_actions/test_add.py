import shutil

from linky import actions
from linky.config import read_conf
from linky.utils.path_utils import BASE_NAME
from tests import TEST_DATA_PATH
from tests.base_test import BaseTest


# pylint: disable=missing-function-docstring
class TestAdd(BaseTest):
    """
    Make sure the actions/add.py isn't doing anything weird
    """

    def setUp(self) -> None:
        BaseTest.setUp(self)
        self.test_data_dir = TEST_DATA_PATH / "add"

    def _test_add(self, rel_file, expected_item,
                  data_dir="simple",
                  linked_root_prefix=False,
                  dupe_handler_name=None,
                  copy=True):
        if copy:
            shutil.copytree(
                self.test_data_dir / data_dir,
                self.test_dir,
                symlinks=True,
            )
        file2 = self.test_dir / rel_file
        if not file2.exists():
            file2.write_text("")
        actions.add(
            file2, self.base_path, read_conf(self.base_path),
            linked_root_prefix=linked_root_prefix,
            dupe_handler_name=dupe_handler_name
        )

        self.assertExists(expected_item)
        if file2.is_file():
            self.assertTrue(expected_item.is_symlink())
            target = expected_item.resolve()
            self.assertExists(target)
        self.assertFilesAreGoodSymlinks(self.base_path.parent)

    def test_with_linked_root_prefix(self):
        rel_path = "Watched/Watched/no_subs/justafile"
        self._test_add(rel_path, self.test_dir / rel_path,
                       linked_root_prefix=True,
                       data_dir="categories")

    def test_with_linked_root_prefix_no_subdir(self):
        rel_path = "Watched/Watched/file2"
        self._test_add(rel_path, self.test_dir / rel_path,
                       linked_root_prefix=True,
                       data_dir="categories")

    def test_in_subdir(self):
        self._test_add("dir/subdir/file2", self.test_dir / "file2")

    def _test_add_external(self, filename):
        self.base_path = self.test_dir / "linked_root" / BASE_NAME
        self._test_add(
            filename,
            self.test_dir / "linked_root" / filename,
            data_dir="external"
        )

    def test_external_file(self):
        self._test_add_external("external_file")

    def test_external_dir(self):
        self._test_add_external("external_dir")

    def test_external_dir_with_linked_root(self):
        """
        Passing linked_root_prefix=True to an external item
        should raise an exception
        """
        filename = "external_dir"
        self.base_path = self.test_dir / "linked_root" / BASE_NAME
        self.assertRaisesRegex(
            ValueError, "Cannot target path outside of root",
            self._test_add,
            filename,
            self.test_dir / "linked_root" / filename,
            data_dir="external",
            linked_root_prefix=True
        )

    def _prepare_simple_dupe_test(self):
        shutil.copytree(
            self.test_data_dir / "simple",
            self.test_dir,
            symlinks=True,
        )
        filename = "dir/dir"
        # Create dir with same name
        directory = self.test_dir / filename
        directory.mkdir()
        (directory / "file").write_text("")
        return filename

    def test_add_duplicate(self):
        filename = self._prepare_simple_dupe_test()

        self.assertRaisesRegex(
            ValueError, "Destination exists. Please choose a dupe handler",
            self._test_add,
            filename,
            None,  # Doesn't matter it shouldn't get that far
            copy=False
        )

    def test_add_duplicate_merge(self):
        filename = self._prepare_simple_dupe_test()

        self._test_add(
            filename,
            self.test_dir / "dir/file",
            dupe_handler_name="merge",
            copy=False
        )
        self._compare_test_dir(self.test_data_dir / "simple_after_merge")
