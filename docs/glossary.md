# Base (directory)

The directory in the [Linked Root][] named `.base` that contains all the data.
All linked directories will reference data in the base.

# Category

An umbrella classification for tags e.g Rating, Actors, Year, etc.

Also called Tag Groups

# Linked Root

This is a root directory managed by linky.

# Tag

Tags can be assigned to any directory or file in the [Linked Root][].

See the document on [Tagging][] for more information.

# Tag Group

See [Category][]

# Uninitialized folder

This is a folder not yet managed by linky / not a [Linked Root][] yet.

[Category]: #category
[Linked Root]: #linked-directory
[Tagging]: ./tagging.md
