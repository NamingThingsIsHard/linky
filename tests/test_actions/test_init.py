import shutil

import linky.config
from linky.actions import init
from tests import TEST_DATA_PATH
from tests.base_test import BaseTest


# pylint: disable=missing-function-docstring
class TestInit(BaseTest):
    """
    Make sure actions/init.py is working alright
    """

    def test_simple_init(self):
        noinit_path = TEST_DATA_PATH / "to_init"
        shutil.copytree(noinit_path, self.test_dir)
        init(self.test_dir)

        self._compare_test_dir("initialized")
        self.assertFilesAreGoodSymlinks(self.test_dir)

    def test_simple_prefixed_init(self):
        noinit_path = TEST_DATA_PATH / "to_init_stdprefix"
        shutil.copytree(noinit_path, self.test_dir)
        init(self.test_dir)

        self.assertFilesAreGoodSymlinks(self.test_dir)
        self._compare_test_dir("initialized_with_prefix")


# pylint: disable=missing-function-docstring
class TestInitWithCategories(BaseTest):
    """
    Testing actions/init.py with pre-categorized folders
    """

    def setUp(self) -> None:
        BaseTest.setUp(self)
        self.test_data_dir = TEST_DATA_PATH / "init" / "with_categories"

    def _test_init_with_categories(self, data_dir, expected_dir, ignore_list=None):
        shutil.copytree(
            self.test_data_dir / data_dir,
            self.test_dir
        )
        init(self.test_dir)
        config = linky.config.read_conf(self.test_dir)
        for category in config.categories.keys():
            self.assertExists(self.test_dir / category)
        self._compare_test_dir(expected_dir)
        self.assertFilesAreGoodSymlinks(self.test_dir, ignore_list=ignore_list)

    def test_only_files(self):
        self._test_init_with_categories("uninit_only_files", "init_only_files")

    def test_already_categorized(self):
        self._test_init_with_categories("uninit_already_categorized", "init_already_categorized")

    def test_already_categorized_with_dupes(self):
        self._test_init_with_categories(
            "uninit_already_categorized_with_dupes", "init_already_categorized_with_dupes", [
                self.test_dir / "Watched/Unwatched/no_subs/in_both",
                self.test_dir / "Watched/Watched/no_subs/in_both",
            ]
        )

    def test_prefixed_init_with_watched(self):
        self._test_init_with_categories(
            "uninit_stdprefix_already_categorized",
            "init_prefixed_already_categorized"
        )
