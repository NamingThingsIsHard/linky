# Presentation of linky

# Start docker
docker run --rm -it \
    -v `pwd`/data:/tmp/presentation \
    -w /tmp/presentation \
    python:3.6-alpine \
    /bin/sh

# Small preparations
apk add --no-cache tree vim
alias ll='ls -lha'
alias ..='cd ..'

# Project page
# ------------------------------------------
# https://gitlab.com/NamingThingsIsHard/linky

# Install linky
# ------------------------------------------
pip install linky-db

# Example 1: Unorganized movie collection
# ------------------------------------------
#  - don't know if watched or not
#  - don't know the actors, year, or rating

cd /tmp/presentation/1.movies

# What things look like before
tree

# What we want is to tag
# Tags can be grouped together
# --> tag groups --> categories

# Categories
# ------------------------------------------
cd .linky
ll
vim categories.yaml

# Initialization / linking
# ------------------------------------------
cd ..
linky init .

# Result
# ------------------------------------------
# Everything is in default categories  we defined
tree -a | less

# Tagging as "Watched"
# ------------------------------------------
linky help tag
cd Status/Unwatched
linky tag -t Status/Watched "Budapest Hotel"

# Not in folder anymore
ll

# Will be in Watched folder
cd ..
ll
cd Watched

# Rating and adding multiple tags
# Movie was Horrible, Weird and Colorful
linky tag \
    -t Rating/Horrible \
    -t Tags/Weird \
    -t Tags/Colorful \
    "Budapest Hotel"

# Result
cd /tmp/presentation/1.movies
find -name "Budapest Hotel"

# ------------------------------------------
# 2. More organized TV series collection
# ------------------------------------------
cd 2.tvseries_categorized
ll
tree -a | less

# Same categories
vim .linky/categories.yaml

# Move tag-folders into a category folder
mkdir Status
mv Unwatched Watched Status/

# Initialization
linky init .

# Result
ll
tree -a | less

# Tag Dark as Creepy, Gloomy, German, Disturbing
linky tag \
    -t Tags/Creepy \
    -t Tags/Gloomy \
    -t Tags/German \
    -t Tags/Disturbing \
    'Status/Unwatched/Dark/S01E05.mkv'

# Mark an episode as Watched
linky tag -t Status/Watched 'Status/Unwatched/Dark/S01E05.mkv'
find -name "S01E05.mkv"

# Rate an entire series as Good and Watched
linky tag \
    -t Rating/Good \
    -t Status/Watched \
    "Rating/Unrated/The Big Bang Theory"
