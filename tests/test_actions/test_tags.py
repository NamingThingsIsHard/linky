from pathlib import Path

from linky.actions.tags import get_tags
from tests.base_test import FixtureBaseTest


# pylint: disable=missing-function-docstring
class TestTags(FixtureBaseTest):
    """
    Tests for actions/tag.py
    """

    def test_get_tags(self):
        hurt_locker = "The Hurt Locker (2008)"
        tag_paths = get_tags(self.config, self.test_dir / "Watched" / "Watched" / hurt_locker)
        self.assertEqual(
            tag_paths, [
                self.test_dir / "Actors" / "Anthony Mackie" / hurt_locker,
                self.test_dir / "Actors" / "Guy Pearce" / hurt_locker,
                self.test_dir / "Actors" / "Jeremy Renner" / hurt_locker,
                self.test_dir / "Watched" / "Watched" / hurt_locker,
                self.test_dir / "Year" / "2008" / hurt_locker,
            ]
        )

    def test_get_tags__multiple_dirs(self):
        hurt_locker = "The Hurt Locker (2008)"
        mashup = "Starcraft - Voldemort and the Sith (2020)"
        tag_paths = get_tags(
            self.config,
            self.test_dir / "Watched" / "Watched" / hurt_locker,
            self.test_dir / "Actors" / "None" / mashup,
        )
        self.assertEqual(
            tag_paths, [
                self.test_dir / "Actors" / "Anthony Mackie" / hurt_locker,
                self.test_dir / "Actors" / "Guy Pearce" / hurt_locker,
                self.test_dir / "Actors" / "Jeremy Renner" / hurt_locker,
                self.test_dir / "Actors" / "None" / mashup,
                self.test_dir / "Watched" / "Unwatched" / mashup,
                self.test_dir / "Watched" / "Watched" / hurt_locker,
                self.test_dir / "Year" / "2008" / hurt_locker,
                self.test_dir / "Year" / "2020" / mashup,
            ]
        )

    def test_get_tags__bad_dirs(self):
        bad_dirs = [
            (AttributeError, None),
            (ValueError, Path("/tmp")),  # external dir
            (ValueError, Path("/nonexistent dir")),  # external dir
            (ValueError, self.test_dir),  # linked root
            (ValueError, self.test_dir / "Actors"),  # tag group dir
            (ValueError, self.test_dir / "Actors" / "Anthony Mackie"),  # tag dir
        ]
        for exception, bad_dir in bad_dirs:
            with self.subTest("Expect exception for bad path", dir=bad_dir, exc=exception):
                self.assertRaises(
                    exception,
                    get_tags,
                    self.config,
                    bad_dir,
                )

    def test_get_tags__good_and_bad_dir(self):
        """
        At the first sign of a bad dir, the function should bail
        """
        self.assertRaises(
            ValueError,
            get_tags,
            self.config,
            self.test_dir / "Actors" / "Anthony Mackie" / "The Hurt Locker (2008)",
            Path("/somewhere"),
        )
