from collections import namedtuple
from pathlib import Path

from linky.actions import move
from linky.utils import path_utils
from tests.base_test import FixtureBaseTest

MoveTestTuple = namedtuple("MoveTestTuple", ("expected_file", "expected_new_links"))


# pylint: disable=missing-function-docstring
class TestMove(FixtureBaseTest):
    """
    Make sure actions/move.py isn't doing shenanigans
    """

    def _test_move(self, *rel_paths, old_root=None, new_root=None):
        """
        Test moving a file/dir within or without the base

        @type rel_paths: Path
        @type old_root: Path
        @type new_root: Path
        """
        old_root = old_root or self.base_path
        new_root = new_root or self.base_path

        force_target_dir = len(rel_paths) > 2

        paths_to_move_d = {}
        rel_source_paths = rel_paths[:-1]
        rel_target_path = rel_paths[-1]
        target = new_root / rel_target_path

        # Prepare a tuple for each source
        for rel_source_path in rel_source_paths:
            self._prepare_move_test(
                rel_source_path, rel_target_path,
                old_root, target,
                paths_to_move_d, force_target_dir
            )

        move(*[*paths_to_move_d.keys(), target])

        # Check each moved source
        for to_move, move_tuple in paths_to_move_d.items():
            self._check_moved_source(
                to_move,
                move_tuple.expected_file, move_tuple.expected_new_links
            )

    def _prepare_move_test(self, rel_source_path, rel_target_path, old_root, target,
                           paths_to_move_d, force_target_dir):
        to_move = path_utils.normpath(old_root / rel_source_path)
        expected_file = target
        if expected_file.is_dir() or force_target_dir:
            expected_file = expected_file / to_move.name
        expected_file = path_utils.normpath(expected_file)
        other_links = path_utils.get_paths_in_root(to_move, self.config)

        self.assertTrue(to_move.exists())
        self.assertFalse(expected_file.exists())
        # All other instances that should exist after the move
        expected_new_links = []

        for other_link in other_links:
            dir_root = Path(
                *other_link.parts[:-len(rel_source_path.parts)]
            )
            expected_new_link = dir_root / rel_target_path
            if expected_new_link.is_dir():
                expected_new_link = expected_new_link / to_move.name
            expected_new_links.append((
                path_utils.normpath(other_link), path_utils.normpath(expected_new_link)
            ))
        paths_to_move_d[to_move] = MoveTestTuple(expected_file, expected_new_links)

    def _check_moved_source(self, to_move, expected_file, expected_new_links):
        self.assertExists(to_move, False)
        self.assertExists(expected_file)

        # Test that all other instances were renamed/moved too
        for other_link, expected_new_link in expected_new_links:
            self.assertFalse(other_link.exists(), "Old file still exists: %s" % other_link)
            self.assertTrue(expected_new_link.exists(),
                            "New file doesn't exist: %s" % expected_new_link)
        self.assertFilesAreGoodSymlinks(self.test_dir)

    def test_should_pass_rename_file_in_base(self):
        movie_dir = Path("The Hurt Locker (2008)")
        self._test_move(
            movie_dir / "The.Hurt.Locker.2008.1080p.BRrip.mkv",
            movie_dir / "Dat_Hurt_Thing_Bruh.mkv",
        )

    def test_should_pass_move_file_in_base(self):
        movie_dir = Path("The Hurt Locker (2008)")
        self._test_move(
            movie_dir / "The.Hurt.Locker.2008.1080p.BRrip.mkv",
            Path("Hurt For Real.mkv"),
        )

    def test_should_pass_move_file_up_one_dir(self):
        movie_dir = Path("The Hurt Locker (2008)")
        self._test_move(
            movie_dir / "The.Hurt.Locker.2008.1080p.BRrip.mkv",
            movie_dir / "..",
        )
        # Make sure the old directory doesn't exist anymore
        self.assertExists(self.test_dir / path_utils.BASE_NAME / movie_dir, False)

    def test_should_pass_move_dir(self):
        self._test_move(
            Path("The Hurt Locker (2008)"),
            Path("2008"),
        )

    def test_should_pass_move_multiple_files(self):
        self._test_move(
            Path("Uncanny Valley (2015)/Uncanny.Valley.1080p.mp4"),
            Path("Starcraft - Voldemort and the Sith (2020)/kekw.mp4"),
            Path("The Hurt Locker (2008)/The.Hurt.Locker.2008.1080p.BRrip.mkv"),
            Path("mp4s")
        )

    def test_should_pass_move_multiple_dirs(self):
        self._test_move(
            Path("Uncanny Valley (2015)"),
            Path("Starcraft - Voldemort and the Sith (2020)"),
            Path("The Hurt Locker (2008)"),
            Path("films")
        )

    def test_should_fail_move_outside_linked_root(self):
        self.assertRaisesRegex(
            ValueError, "Trying to move in or out of base",
            self._test_move,
            Path("The Hurt Locker (2008)"),
            Path("2008"),
            new_root=Path("/tmp")
        )

    def test_should_fail_move_outside_base(self):
        self.assertRaisesRegex(
            ValueError, "Trying to move in or out of base",
            self._test_move,
            Path("The Hurt Locker (2008)"),
            Path("2008"),
            new_root=self.base_path.parent
        )

    def test_should_fail_move_outside_tag(self):
        self.assertRaisesRegex(
            ValueError, "Cannot switch categories or tag using move",
            self._test_move,
            Path("The Hurt Locker (2008)"),
            Path("2008"),
            old_root=self.base_path.parent / "Actors" / "Jeremy Renner",
            new_root=self.base_path.parent / "Actors" / "Another Actor"
        )

    def test_should_fail_move_outside_category(self):
        self.assertRaisesRegex(
            ValueError, "Cannot categorize using move",
            self._test_move,
            Path("The Hurt Locker (2008)"),
            Path("2008"),
            old_root=self.base_path.parent / "Actors" / "Jeremy Renner",
            new_root=self.base_path.parent
        )
